import React, { useState, useEffect } from 'react';
// by default runs after every re-render
// cleanup function
// second parameter
const UseEffectBasics = () => {
  const [value, setValue] = useState(0);

  const [text, setText] = useState("testing");

  useEffect(() => {
    console.log('call useEffect');
    if (value > 0) {
      document.title = `${text})(${value})`;
    }
  });

  

  console.log('render component');
  return (
    <>
      <h1>{value}</h1>
      <h1>{text}</h1>
      <button className='btn' onClick={() => setValue(value + 1)}>
        click me
      </button>
    </>
  );
};

export default UseEffectBasics;
