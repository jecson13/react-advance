import React, { useState, useEffect } from 'react'
import ErrorExample from './tutorial/1-useState/final/1-error-example'
import UseEffectBasics from './tutorial/2-useEffect/final/1-useEffect-basics'
// import UseStateArray from './tutorial/1-useState/final/3-useState-array'
// import UseStateObject from './tutorial/1-useState/final/4-useState-object'
// import UseStateCounter from './tutorial/1-useState/final/5-useState-counter'
import UseEffectCleanup from './tutorial/2-useEffect/final/2-useEffect-cleanup'
import UseEffectFetchData from './tutorial/2-useEffect/final/3-useEffect-fetch-data'
// import MultipleReturns from './tutorial/3-conditional-rendering/final/1-multiple-returns'
import ControlledInputs from "./tutorial/4-forms/final/2-multiple-inputs";
import MultipleReturns from "./tutorial/4-forms/final/2-multiple-inputs";
import UseRefBasics from './tutorial/5-useRef/final/1-useRef-basics'


function App() {

  const [isShowEffect, setIsShowEffect] = useState(false);
  const [isMultipleReturns, setIsMultipleReturns] = useState(false);

  useEffect(() => {
    console.log("Effect button is clickedd")
    
  }, [isShowEffect])

  return (
    <>
      {/* <ErrorExample/>
      <UseStateBasics/>
      
      <UseStateArray/>
      <UseStateObject/>
      <UseStateCounter/> */}

      {/* <UseEffectBasics/> */}
      <button className='btn' onClick={() => setIsShowEffect(!isShowEffect)}>
          {!isShowEffect ? 'Show Effect' : 'Hide Effect'}
      </button>
      {isShowEffect && <UseEffectCleanup/>}


      {/* <button className='btn' onClick={() => setIsMultipleReturns(!isMultipleReturns)}>
          {!isShowEffect ? 'Show Multiple Returns' : 'Hide Multiple Returns'}
      </button>
      {isMultipleReturns && <MultipleReturns/>} */}
      <ControlledInputs/>
      <MultipleReturns/>
      <UseRefBasics/>
      
      
    </>
  )
}

export default App
